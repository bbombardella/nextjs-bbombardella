import Head from 'next/head'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import WindowTab from '../WindowTab'
import 'bootstrap/dist/css/bootstrap.min.css'
import styles from '../../styles/Layout.module.scss'
import Header from '../Header'
import {ReactNode} from "react";

export default function Layout({title = "Bastien", children}: { title: string, children: ReactNode }) {
    return (
        <>
            <Head>
                <title>{title}</title>
            </Head>

            <Container className={styles.container}>
                <Row className={styles.row}>
                    <Col xs={12} className={styles.column}>
                        <WindowTab/>
                        <Header/>
                        <Row className={styles['row-main']}>
                            <Col className={styles['col-main']}>
                                {children}
                            </Col>
                        </Row>

                    </Col>
                </Row>
            </Container>
        </>
    )
}
