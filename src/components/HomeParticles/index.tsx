import React, {useCallback} from "react"
import Typewriter from 'typewriter-effect'
import {loadFull} from "tsparticles"
import Particles from "react-particles";
import {settings} from './settings';
import strings from './strings.json';
import styles from '../../styles/Particles.module.scss';
import {Container, Engine} from "tsparticles-engine";

export default function HomeParticles() {
    const customInit = useCallback(async (engine: Engine) => {
        await loadFull(engine);
    }, []);

    const typeWritterOptions = {
        className: styles.text,
        options: {strings: strings, autoStart: true, loop: true, cursor: "_"}
    };

    return (
        <div className={styles.wrapper}>
            <span className={styles.text}>Je suis<span> </span>
                <Typewriter {...typeWritterOptions} />
            </span>
            <Particles className={styles.particles} init={customInit} options={settings}/>
        </div>
    )
}
