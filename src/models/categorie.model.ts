import {Competence} from "./competence.model";

export interface Categorie {
    id: number;
    name: string;
    competences: Competence[];
}
