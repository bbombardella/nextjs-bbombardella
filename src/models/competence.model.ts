import {Categorie} from "./categorie.model";

export interface Competence {
    id: number;
    name: string;
    pourcentage?: number;
    icon: any;
    image?: string;
    catcompetences: Categorie[];
}
