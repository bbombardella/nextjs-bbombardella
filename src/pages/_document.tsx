import {Html, Head, Main, NextScript} from 'next/document'

export default function Document() {
    return (
        <Html>
            <Head>
                <link href="https://fonts.googleapis.com/css2?family=Fjalla+One&display=swap" rel="stylesheet"/>
                <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
                <meta property="og:image" content="https://www.bastienbc.fr/seo.png"/>
                <meta property="twitter:image" content="https://www.bastienbc.fr/seo.png"/>
            </Head>
            <body>
                <Main/>
                <NextScript/>
            </body>
        </Html>
    )
}
