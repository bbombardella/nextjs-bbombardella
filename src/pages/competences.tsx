import React, {useState} from 'react'
import Head from 'next/head'
import Layout from '../components/Layout'
import CompetenceCard from '../components/CompetenceCard'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import {AnimatePresence, motion} from 'framer-motion'
import styles from '../styles/Competences.module.scss'
import 'bootstrap/dist/css/bootstrap.min.css'

import {Competence} from "../models/competence.model";
import {Categorie} from "../models/categorie.model";

export default function Competences({competences, categories}: { competences: Competence[], categories: Categorie[] }) {

    const [comp, setComp] = useState<Competence[]>(competences);
    const [isLoading, setisLoading] = useState<boolean>(false);

    function handleFilter(filter: Categorie | 'Tous') {
        setisLoading(true);

        if (filter === "Tous") {
            setComp(competences);
        } else {
            setComp(competences?.filter(competence => competence?.catcompetences?.some(categorie => categorie?.id === filter?.id)) ?? []);
        }

        setTimeout(() => {
            setisLoading(false)
        }, 500)
    }

    return (
        <>
            <Head>
                <title>Compétences — Bastien Bombardella</title>
                <meta name="title" content="Compétences — Bastien Bombardella"/>
                <meta name="description" content="Retrouvez toutes mes compétences dans cette page !"/>

                <meta property="og:type" content="website"/>
                <meta property="og:url" content="https://www.bastienbc.fr/competences"/>
                <meta property="og:title" content="Compétences — Bastien Bombardella"/>
                <meta property="og:description" content="Retrouvez toutes mes compétences dans cette page !"/>

                <meta property="twitter:card" content="summary_large_image"/>
                <meta property="twitter:url" content="https://www.bastienbc.fr/competences"/>
                <meta property="twitter:title" content="Compétences — Bastien Bombardella"/>
                <meta property="twitter:description" content="Retrouvez toutes mes compétences dans cette page !"/>
            </Head>
            <Layout title="Compétences — Bastien Bombardella">
                <main>
                    <Row>
                        <Col xs={12}>
                            <h1 className={styles.title}>Mes compétences</h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <ul className={styles.categories}>
                                <li onClick={() => {
                                    handleFilter("Tous");
                                }}>Tous
                                </li>
                                {categories.map((categorie, index) => (
                                    <li key={index} onClick={() => {
                                        handleFilter(categorie);
                                    }}>{categorie.name}</li>
                                ))}
                            </ul>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <AnimatePresence onExitComplete={() => {
                            }}>
                                {!isLoading &&
                                  <motion.ul exit={{opacity: 0}} className={styles.competences}>
                                      {comp.map((competence, index) => (
                                          <CompetenceCard key={index} data={competence}/>
                                      ))}
                                  </motion.ul>
                                }
                            </AnimatePresence>
                        </Col>
                    </Row>
                </main>
            </Layout>
        </>
    )
}

export async function getStaticProps() {
    const competences: Competence[] = await (await fetch(`${process.env.api}competences`)).json() ?? [];

    if (Array.isArray(competences) && competences?.length > 0) {
        for (const competence of competences) {
            if (competence?.icon?.url) {
                const image = await fetch(`${process.env.api}${competence.icon.url}`);
                competence.image = `data:${competence?.icon?.mime ?? 'image/jpeg'};base64,${btoa(String.fromCharCode(...new Uint8Array(await image.arrayBuffer())))}`;
            }
        }
    }

    const categories: Categorie[] = await (await fetch(`${process.env.api}catcompetences`)).json() ?? [];

    return {
        props: {
            competences,
            categories
        },
    }
}
