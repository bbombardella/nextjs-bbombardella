module.exports = {
  reactStrictMode: true,
  env: {
    api: "https://strapi.bastienbc.fr/"
  },
  images: {
    loader: 'akamai',
    path: ' ',
    domains: [
      'strapi.bastienbc.fr',
      'bastienbc.fr'
    ]
  },
  trailingSlash: true
}
